<?php
/**
 * Created by PhpStorm.
 * User: ADMIN
 * Date: 27/11/2015
 * Time: 7:53 CH
 */
        ?>
@extends('admin.layout')
@section('content')
  <div class="container-fluid">
    <div class="row page-title-row">
      <div class="col-md-6">
        <h3>Roles</small></h3>
      </div>
      <div class="col-md-6 text-right">
        <a href="/admin/role/create" class="btn btn-success btn-md">
          <i class="fa fa-plus-circle"></i> New create
        </a>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-12">
<!--
        @include('admin.partials.errors')
        @include('admin.partials.success') -->

        <table id="tags-table" class="table table-striped table-bordered">
          <thead>
          <tr>
            <th>Role name</th>
            <th data-sortable="false">Actions</th>
          </tr>
          </thead>
          <tbody>
          @foreach ($roles as $role)
            <tr>
              <td>{{ $role->role_name }}</td>
              <td>
                <a href="/admin/role/{{ $role->id }}/edit"
                   class="btn btn-xs btn-info">
                  <i class="fa fa-edit"></i> Edit
                </a>
              </td>
            </tr>
          @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
@stop

@section('scripts')
  <script>
    $(function() {
      $("#tags-table").DataTable({
      });
    });
  </script>
@stop

