<?php
/**
 * Created by PhpStorm.
 * User: ADMIN
 * Date: 16/11/2015
 * Time: 8:49 CH
 */
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>phpwork - Admin</title>

        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"
              rel="stylesheet">
        @yield('styles')

        <!--[if lt IE 9]>
          <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
          <![endif]-->

        <script src="//js.pusher.com/3.0/pusher.min.js"></script>
        <script>
            var pusher = new Pusher("{{env("PUSHER_KEY")}}")
            var channel = pusher.subscribe('admin');
            channel.bind('insert', function (data) {
                alert(data.text);
            });
            channel.bind('delete', function (data) {
                alert(data.text);
            });
            channel.bind('update', function (data) {
                alert(data.text);
            });
        </script>
    </head>
    <body>

        {{-- Navigation Bar --}}
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed"
                            data-toggle="collapse" data-target="#navbar-menu">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">{{ config('blog.title') }} Admin</a>
                    <ul class="nav navbar-nav">
                        <li @if (Request::is('admin/post')) class="active" @endif>
                             <a href="/admin/post">Manage Post</a>
                        </li>
                        <li @if (Request::is('admin/user')) class="active" @endif>
                             <a href="/admin/user">Manage User</a>
                        </li>
                        <li @if (Request::is('admin/role')) class="active" @endif>
                             <a href="/admin/role">Manage Role</a>
                        </li>
                        <li @if (Request::is('admin/permission')) class="active" @endif>
                             <a href="/admin/permission">Manage Permission</a>
                        </li>
                    </ul>
                </div>
                <div class="collapse navbar-collapse" id="navbar-menu">
                    @include('admin.partials.navbar')
                </div>
            </div>
        </nav>

        @yield('content')

        <script
        src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script
        src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

        @yield('scripts')

    </body>
</html>
