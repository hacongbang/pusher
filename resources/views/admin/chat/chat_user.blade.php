<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>phpwork - Admin</title>

        <meta name="csrf-token" content="{{ csrf_token() }}" />

        <link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,200italic,300italic" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="http://d3dhju7igb20wy.cloudfront.net/assets/0-4-0/all-the-things.css" />
        <style>
            .chat-app {
                margin: 50px;
                padding-top: 10px;
            }

            .chat-app .message:first-child {
                margin-top: 15px;
            }

            #messages {
                height: 300px;
                overflow: auto;
                padding-top: 5px;
            }
        </style>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"
              rel="stylesheet">
        <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
        <script src="https://cdn.rawgit.com/samsonjs/strftime/master/strftime-min.js"></script>
        <script src="//js.pusher.com/3.0/pusher.min.js"></script>

        <script>
    // Ensure CSRF token is sent with AJAX requests
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // Added Pusher logging
    Pusher.log = function (msg) {
        console.log(msg);
    };
        </script>
    </head>
    <body>
        {{-- Navigation Bar --}}
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed"
                            data-toggle="collapse" data-target="#navbar-menu">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">{{ config('blog.title') }} Admin</a>
                    <ul class="nav navbar-nav">
                        <li @if (Request::is('admin/post')) class="active" @endif>
                             <a href="/admin/post">Manage Post</a>
                        </li>
                        <li @if (Request::is('admin/user')) class="active" @endif>
                             <a href="/admin/user">Manage User</a>
                        </li>
                        <li @if (Request::is('admin/role')) class="active" @endif>
                             <a href="/admin/role">Manage Role</a>
                        </li>
                        <li @if (Request::is('admin/permission')) class="active" @endif>
                             <a href="/admin/permission">Manage Permission</a>
                        </li>
                        <li @if (Request::is('chat')) class="active" @endif>
                             <a href="/chat">Chat Group</a>
                        </li>
                    </ul>
                </div>
                <div class="collapse navbar-collapse" id="navbar-menu">
                    @include('admin.partials.navbar')
                </div>
            </div>
        </nav>
        <section class="blue-gradient-background">
            <div class="container">
                <div class="row light-grey-blue-background chat-app">

                    <div id="messages">
                        <div class="time-divide">

                        </div>
                    </div>

                    <div class="action-bar">
                        <textarea class="input-message col-xs-10" placeholder="Your message"></textarea>
                        <div class="option col-xs-1 white-background">
                            <span class="fa fa-smile-o light-grey"></span>
                        </div>
                        <div class="option col-xs-1 green-background send-message">
                            <span class="white light fa fa-paper-plane-o"></span>
                        </div>
                    </div>

                </div>
            </div>
        </section>

        <script id="chat_message_template" type="text/template">
            <div class="message">
            <div class="avatar">
            <img src="">
            </div>
            <div class="text-display">
            <div class="message-data">
            <span class="author"></span>
            <span class="timestamp"></span>
            <span class="seen"></span>
            </div>
            <p class="message-body"></p>
            </div>
            </div>
        </script>

        <script>
            function init() {
                // send button click handling
                $('.send-message').click(sendMessage);
                $('.input-message').keypress(checkSend);
            }

            // Send on enter/return key
            function checkSend(e) {
                if (e.keyCode === 13) {
                    return sendMessage();
                }
            }

            // Handle the send button being clicked
            function sendMessage() {
                var messageText = $('.input-message').val();
                if (messageText.length < 3) {
                    return false;
                }

                // Build POST data and make AJAX request
                var data = {chat_text: messageText};
                $.post('/chat/messageuser/{{$chatChannel}}', data).success(sendMessageSuccess);

                // Ensure the normal browser event doesn't take place
                return false;
            }

            // Handle the success callback
            function sendMessageSuccess() {
                $('.input-message').val('')
                console.log('message sent successfully');
            }

            // Build the UI for a new message and add to the DOM
            function addMessage(data) {
                // Create element from template and set values
                var el = createMessageEl();
                el.find('.message-body').html(data.text);
                el.find('.author').text(data.username);
                el.find('.avatar img').attr('src', data.avatar)

                // Utility to build nicely formatted time
                el.find('.timestamp').text(strftime('%H:%M:%S %P', new Date(data.timestamp)));

                var messages = $('#messages');
                messages.append(el)

                // Make sure the incoming message is shown
                messages.scrollTop(messages[0].scrollHeight);
            }

            // Creates an activity element from the template
            function createMessageEl() {
                var text = $('#chat_message_template').text();
                var el = $(text);
                return el;
            }

            $(init);

            /***********************************************/

            var pusher = new Pusher('{{env("PUSHER_KEY")}}');

            var channel = pusher.subscribe('{{$chatChannel}}');
            channel.bind('new-message', addMessage);

        </script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    </body>
</html>
