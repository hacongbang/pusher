@extends('admin.layout')
@section('content')
<div class="container-fluid">
    <div class="row page-title-row">
        <div class="col-md-6">
            <h3>Posts</small></h3>
        </div>
        <div class="col-md-6 text-right">
            <a href="/admin/post/create" class="btn btn-success btn-md">
                <i class="fa fa-plus-circle"></i> New post
            </a>
        </div>
    </div>
    <table id="tags-table" class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>ID</th>
                <th>Slug</th>
                <th class="hidden-sm">Title</th>
                <th class="hidden-md">Content</th>
                <th class="hidden-md">Created at</th>
                <th class="hidden-md">Update at</th>
                <th class="hidden-sm">Pushlished at</th>
                <th class="hidden-sm">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($posts as $post)
            <tr>
                <td>{{ $post->id }}</td>
                <td>{{ $post->slug }}</td>
                <td class="hidden-sm">{{ $post->title }}</td>
                <td class="hidden-md">{{ substr($post->content,0,100) }}</td>
                <td class="hidden-md">{{ $post->created_at }}</td>
                <td class="hidden-md">{{ $post->updated_at }}</td>
                <td class="hidden-md">{{ $post->published_at }}</td>
                <td class="hidden-md"><a href="/admin/post/{{ $post->id }}/edit">Edit</a></td>
            </tr>
            @endforeach
        </tbody>

    </table>
</div>
{!! $posts->render() !!}
@stop