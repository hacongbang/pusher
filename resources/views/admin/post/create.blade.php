@extends('admin.layout')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">New Post Form</h3>
    </div>
    <div class="panel-body">

        @include('admin.partials.errors')

        <form class="form-horizontal" role="form" method="POST"
              action="/admin/post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            @include('admin.post._form')

            <div class="form-group">
                <div class="col-md-7 col-md-offset-3">
                    <button type="submit" class="btn btn-primary btn-md">
                        <i class="fa fa-plus-circle"></i>
                        Add New Post
                    </button>
                </div>
            </div>

        </form>

    </div>
</div>
@stop