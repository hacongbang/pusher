@extends('admin.layout')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">New Post Form</h3>
    </div>
    <div class="panel-body">

        @include('admin.partials.errors')

        <form class="form-horizontal" role="form" method="POST"
              action="{{ route('admin.post.update',$id) }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="_method" value="PUT">
            <input type="hidden" name="id" value="{{ $id }}">

            @include('admin.post._form')

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary btn-lg">Edit role</button>
                    <button type="button" class="btn btn-danger btn-lg"
                            data-toggle="modal" data-target="#modal-delete">
                        <i class="fa fa-times-circle"></i>
                        Delete
                    </button>
                </div>
            </div>

        </form>

    </div>
</div>
{{-- Confirm Delete --}}
<div class="modal fade" id="modal-delete" tabIndex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    ×
                </button>
                <h4 class="modal-title">Please Confirm</h4>
            </div>
            <div class="modal-body">
                <p class="lead">
                    <i class="fa fa-question-circle fa-lg"></i>
                    Are you sure you want to delete this post?
                </p>
            </div>
            <div class="modal-footer">
                <form method="POST" action="{{ route('admin.post.destroy', $id) }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="_method" value="DELETE">
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">
                        <i class="fa fa-times-circle"></i> Yes
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
@stop