
<div class="form-group">
  <label for="title" class="col-md-3 control-label">
    Title
  </label>
  <div class="col-md-8">
    <input type="text" class="form-control" name="title"
           id="title" value="{{ $title }}">
  </div>
</div>

<div class="form-group">
  <label for="title" class="col-md-3 control-label">
    Slug
  </label>
  <div class="col-md-8">
    <input type="text" class="form-control" name="slug"
           id="slug" value="{{ $slug }}">
  </div>
</div>

<div class="form-group">
  <label for="meta_description" class="col-md-3 control-label">
    Content
  </label>
  <div class="col-md-8">
    <textarea class="form-control" id="content"
              name="content" rows="3">{{
      $content
    }}</textarea>
  </div>
</div>
