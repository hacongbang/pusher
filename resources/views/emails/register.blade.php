<?php
/**
 * Created by PhpStorm.
 * User: ADMIN
 * Date: 07/12/2015
 * Time: 11:20 CH
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Email</title>
</head>
<body>

Hello {{ $name }} thanks for registering! <br><br>

<br><br>
Please click the link to verify your registration
<a href="{{ url('/profile/register/activate/'. $code) }}">
{{ url('/profile/register/activate/'. $code) }}
</a><br><br><br><br><br>

Thanks<br><br>

<p style="color:red;">phpwork.</p>
</body>
</html>
