<?php
/**
 * Created by PhpStorm.
 * User: ADMIN
 * Date: 28/11/2015
 * Time: 3:42 CH
 */
        ?>
<?php
/**
 * Created by PhpStorm.
 * User: ADMIN
 * Date: 27/11/2015
 * Time: 7:52 CH
 */
?>
@extends('admin.layout')

@section('content')
    <div class="container-fluid">
        <div class="row page-title-row">
            <div class="col-md-12">
                <h3>Edit profile</h3>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Edit profile</h3>
                    </div>
                    <div class="panel-body">

                        @include('admin.partials.errors')
                        @include('admin.partials.success')

                        <form class="form-horizontal" role="form" method="POST"
                              action="/profile/{{ $id }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="_method" value="PUT">
                            <input type="hidden" name="id" value="{{ $id }}">

                            <div class="form-group">
                                <label for="tag" class="col-md-3 control-label">Hello : </label>
                                <div class="col-md-3">
                                    <p class="form-control-static">{{ $name }}</p>
                                </div>
                            </div>

                            @include('profile.partials._form')

                            <div class="form-group">
                                <div class="col-md-7 col-md-offset-3">
                                    <button type="submit" class="btn btn-primary btn-md">
                                        <i class="fa fa-save"></i>
                                        Save Changes
                                    </button>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

