<?php
/**
 * Created by PhpStorm.
 * User: ADMIN
 * Date: 28/11/2015
 * Time: 12:56 SA
 */
?>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label class="col-md-4 control-label">Name</label>
            <div class="col-md-6">
                <input type="text" class="form-control" name="name" value="{{ $name }}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label">E-Mail Address</label>
            <div class="col-md-6">
                <input type="email" class="form-control" name="email" value="{{ $email }}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label">Password</label>
            <div class="col-md-6">
                <input type="password" class="form-control" name="password">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label">Confirm Password</label>
            <div class="col-md-6">
                <input type="password" class="form-control" name="password_confirmation">
            </div>
        </div>
        <div class="form-group">

            <div class="col-md-6">
                <input type="hidden" class="form-control" name="role_id" value="3" readonly>
            </div>
        </div>

    </div>
    <div class="col-md-8">
        <div class="form-group">
            <label for="firstname" class="col-md-3 control-label">
                First name
            </label>
            <div class="col-md-8">
                <input class="form-control" name="firstname" id="firstname"
                       type="text" value="{{ $firstname }}">
            </div>
        </div>
        <div class="form-group">
            <label for="lastname" class="col-md-3 control-label">
                Last name
            </label>
            <div class="col-md-8">
                <input class="form-control" name="lastname" id="lastname"
                       type="text" value="{{ $lastname }}">
            </div>
        </div>
        <div class="form-group">
            <label for="phone_number" class="col-md-3 control-label">
                Phone nnumber
            </label>
            <div class="col-md-8">
                <input class="form-control" name="phone_number" id="phone_number"
                       type="text" value="{{ $phone_number }}">
            </div>
        </div>
        <div class="form-group">
            <label for="linked" class="col-md-3 control-label">
                Linked
            </label>
            <div class="col-md-8">
                <input class="form-control" name="linked" id="linked"
                       type="text" value="{{ $linked }}">
            </div>
        </div>
        <div class="form-group">
            <label for="addres" class="col-md-3 control-label">
                Addres
            </label>
            <div class="col-md-8">
                <input class="form-control" name="address" id="addres"
                       type="text" value="{{ $address }}">
            </div>
        </div>

    </div>
</div>


