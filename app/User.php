<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Support\Facades\Auth;

class User extends Model implements AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];


    /*
    |--------------------------------------------------------------------------
    | ACL Methods
    |--------------------------------------------------------------------------
    */

    /**
     * Checks a Permission
     *
     * @param  String permission Slug of a permission (i.e: manage_user)
     * @return Boolean true if has permission, otherwise false
     */
    public function hasPermission($permission = null)
    {

        return !is_null($permission) && $this->checkPermission($permission);
    }

    /**
     * Check if the permission matches with any permission user has
     *
     * @param  String permission slug of a permission
     * @return Boolean true if permission exists, otherwise false
     */
    protected function checkPermission($perm)
    {
        
        $roles = $this->getAllRolesOfUser();
        $permissions = $this->getPernissionsFormAllRoles($perm);               
        return count(array_intersect($permissions, $roles));
    }

    /**
     * Get all permission slugs from all permissions of all roles
     *
     * @return Array of permission slugs
     */
    protected function getPernissionsFormAllRoles($slug)
    {
        $per = new Permission();
        
        $permissions = $per->getPermissionBySlug($slug);
        
        $list_roles = $permissions->roles()->lists('roles_user.roles_id')->all();

        return $list_roles;

    }

    /**
     * Get all permission slugs from all permissions of all roles
     *
     * @return Array of permission slugs
     */
    protected function getAllRolesOfUser()
    {
        $roleArray = [];
        $roles = Auth::user()->roles()->lists('roles_user.roles_id')->all();
        return $roles;  
    }

    public function profile()
    {
        return $this->hasOne('App\Profile');
    }

    public function roles()
    {
        return $this->belongsToMany('App\Roles');
    }

    public function fields_user()
    {
        return

            [
                'name' => '',
                'email' => '',
                'password' => '',
            ];
    }

    public function fields_profile()
    {
        return
            [
                'firstname' => '',
                'lastname' => '',
                'phone_number' => '',
                'linked' => '',
                'address' => '',
            ];
    }

    public function isAdmin()
    {

        $roles = Auth::user()->roles()->lists('role_name')->all();
        if (in_array( 'administrator',$roles)){
            return true;
        }else{
            return false;
        }
    }
}
