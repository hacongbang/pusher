<?php

namespace App\Http\Controllers\Admin;

use App\Roles;
use Illuminate\Http\Request;
use App\User;
use App\Profile;
use App\Role;
use App\Jobs\UserFormFields;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Input;
use Laravel\Socialite\Facades\Socialite;
use Redirect;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $rules = [
        'name' => 'required|max:255',
        'email' => 'required|email|max:255|unique:users',
        'password' => 'required|confirmed|min:6',
        'firstname' => 'required|max:255',
        'lastname' => 'required|max:255',
    ];

    public function index()
    {
        $users = User::with('profile')->get();

        return view('admin.user.index', ['users' => $users->toArray()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = $this->dispatch(new UserFormFields());
        return view('admin.user.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->rules);
        $user = new User();
        $profile = new Profile();
        foreach (array_keys($user->fields_user()) as $field) {
            $user->$field = $request->get($field);
        }
        $user->password = bcrypt($user->password);
        $user->save();
        foreach (array_keys($user->fields_profile()) as $field) {
            $profile->$field = $request->get($field);
        }
        $user->roles()->sync($request->get('roles'), []);
        $profile->is_active = 1;
        $user->profile()->save($profile);
        return redirect()
            ->route('admin.user.index')
            ->withSuccess("Hi '$user->name' please create profile.");

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = $this->dispatch(new UserFormFields($id));
        return view('admin.user.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'password' => 'required|confirmed|min:6',
        ]);
        $user = User::find($id);
        $user->profile->get();
        // Save data for table user.
        foreach (array_keys($user->fields_user()) as $field) {
            if ($user->$field != $request->get($field)) {
                $user->$field = $request->get($field);
            }
        }
        // Save password.
        $user->password = bcrypt($user->password);
        // Save data for table profile.
        foreach (array_keys($user->fields_profile()) as $field) {
            $user->profile->$field = $request->get($field);
        }
        $user->save();
        $user->roles()->sync($request->get('roles'), []);
        $user->profile->save();

        return redirect()
            ->route('admin.user.index')
            ->withSuccess("Hi '$user->name' please create profile.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->with('profile')->get();
        $user->profile->delete();
        $user->roles()->detach();
        $user->delete();
        return redirect()
            ->route('admin.user.index')
            ->withSuccess('User deleted.');

    }

//    public function loginWithFacebook()
//    {
//        try {
//            $user = Socialite::driver('facebook')->user();
//        } catch (Exception $e) {
//            return redirect('auth/facebook');
//        }
//
//        $authUser = $this->findOrCreateUser($user);
//
//        Auth::login($authUser, true);
//
//        return redirect('/profile/' . $authUser->id . '/edit')
//            ->withSuccess("Hi '$user->name' please update profile.");
//
//    }
//
//    public function loginWithGoogle()
//    {
//        try {
//            $user = Socialite::driver('google')->user();
//        } catch (Exception $e) {
//            return redirect('auth/google');
//        }
//
//        $authUser = $this->findOrCreateUserByGoogle($user);
//
//        Auth::login($authUser, true);
//
//        return redirect('/profile/' . $authUser->id . '/edit')
//            ->withSuccess("Hi '$user->name' please update profile.");
//
//    }
//
//    public function findOrCreateUser($facebookUser)
//    {
//        $authUser = User::where('email', $facebookUser->email)->first();
//        if ($authUser) {
//            return $authUser;
//        }
//
//        $account = [
//            'name' => $facebookUser->name,
//            'email' => $facebookUser->email,
//            'firstname' => $facebookUser->user['first_name'],
//            'lastname' => $facebookUser->user['last_name'],
//        ];
//        return $this->saveUserLogin($account);
//
//    }
//
//    public function findOrCreateUserByGoogle($googleUser)
//    {
//        $authUser = User::where('email', $googleUser->email)->first();
//        if ($authUser) {
//            return $authUser;
//        }
//        $account = [
//            'name' => $googleUser->name,
//            'email' => $googleUser->email,
//            'firstname' => $googleUser->user['name']['familyName'],
//            'lastname' => $googleUser->user['name']['givenName'],
//        ];
//        return $this->saveUserLogin($account);
//
//    }
//
//    public function saveUserLogin($account)
//    {
//        $user = new User();
//        $profile = new Profile();
//
//        if ($account['email'] == null) {
//            $account['email'] = 'khoa.nguyenluunhat@gmail.com';
//        }
//        $user->name = $account['name'];
//        $user->email = $account['email'];
//        $profile->firstname = $account['firstname'];
//        $profile->lastname = $account['lastname'];
//        $profile->active_code = str_random(60);
//        //  $user->password = bcrypt($user->password);
//        $user->save();
//        $user->roles()->sync([4]);
//        $user->profile()->save($profile);
//        $input = [
//            'name' => $account['name'],
//            'email' => $account['email']
//        ];
//        $data = [
//            'name' => $account['name'],
//            'code' => $profile->active_code
//        ];
//
//        $this->sendEmail($data, $input);
//        return $user;
//    }
//
//
//    public function activeAccount($code)
//    {
//        $code = Crypt::decrypt($code);
//
//        $profile = Profile::where('active_code', $code)->first();
//
//        if ($profile) {
//            $profile->update(['is_active' => 1, 'active_code' => NULL]);
//            $user = User::find($profile->user_id);
//
//            if ($user) {
//                Auth::login($user);
//            }
//            return true;
//        }
//    }
//
//    public function sendEmail($data, $input)
//    {
//        Mail::send('emails . register', $data, function ($message) use ($input) {
//
//            $message->from('daigiangaitu91@gmail.com', 'Laravel - Auth');
//
//            $message->to($input['email'], $input['name'])->subject('Please verify your account registration!');
//
//        });
//
//    }
}
