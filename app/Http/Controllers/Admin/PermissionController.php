<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Jobs\PermissionsFormFields;
use App\Permission;
use App\Roles;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $rules = [
            'permission_title' => 'required',
        ];


    /**
     * Return the fields and values to create a new post from
     */
    public function permissionFillData(Request $request)
    {
        return [
            'permission_title' => $request->permission_title,
            'permission_slug' => $request->permission_slug,
            'permission_description' => $request->permission_description,
        ];
    }

    public function index()
    {
        $permissions = Permission::with('roles')->get();

        return view('admin.permission.index')->withPermissions($permissions->toArray());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = $this->dispatch(new PermissionsFormFields());

        return view('admin.permission.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->rules);
        $permission = Permission::create($this->permissionFillData($request));
        $permission->save();
        $permission->syncRoles($request->get('roles', []));

        return redirect()
            ->route('admin.permission.index')
            ->withSuccess('New Permission Successfully Created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $data = $this->dispatch(new PermissionsFormFields($id));

        $data['id'] = $data['id'];
        return view('admin.permission.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $permission = Permission::find($id);

        $permission->fill($this->permissionFillData($request));
        
        $permission->save();

        $permission->syncRoles($request->get('roles', []));

        return redirect()
            ->route('admin.permission.index')
            ->withSuccess('Permission saved.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $permission = Permission::find($id);
        $permission->roles()->detach();
        $permission->delete();

        return redirect()
            ->route('admin.permission.index')
            ->withSuccess('Permission deleted.');
    }
}
