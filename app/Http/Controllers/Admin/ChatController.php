<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class ChatController extends Controller
{
    var $pusher;
    var $user;
    var $chatChannel;

    const DEFAULT_CHAT_CHANNEL = 'admin';

    public function __construct()
    {
        $this->pusher = App::make('pusher');
        $this->user = Auth::user();
        $this->chatChannel = self::DEFAULT_CHAT_CHANNEL;
    }

    public function getIndex()
    {
        if(!$this->user)
        {
            return redirect('/');
        }
        return view('admin.chat.chat_all', ['chatChannel' => $this->chatChannel]);
    }

    public function postMessage(Request $request)
    {
        $message = [
            'text' => e($request->input('chat_text')),
            'username' => $this->user->name,
            'avatar' => 'https://scontent-hkg3-1.xx.fbcdn.net/hprofile-xfa1/v/t1.0-1/c47.0.160.160/p160x160/10354686_10150004552801856_220367501106153455_n.jpg?oh=0a313444af6a1af3920deebe4242a4e2&oe=57913249',
            'timestamp' => (time()*1000)
        ];
        $this->pusher->trigger($this->chatChannel, 'new-message', $message);
    }
    
    public function getUser($uid)
    {
        if(!$this->user)
        {
            return redirect('/');
        }
       
        $chatchannel_user = implode('', array_sort_recursive([$this->user->id, $uid]));
        return view('admin.chat.chat_user', ['chatChannel' => $chatchannel_user]);
    }
    
    public function postMessageuser(Request $request, $chatchannel_user)
    {
        $message = [
            'text' => e($request->input('chat_text')),
            'username' => $this->user->name,
            'avatar' => 'https://scontent-hkg3-1.xx.fbcdn.net/hprofile-xfa1/v/t1.0-1/c47.0.160.160/p160x160/10354686_10150004552801856_220367501106153455_n.jpg?oh=0a313444af6a1af3920deebe4242a4e2&oe=57913249',
            'timestamp' => (time()*1000)
        ];
        $this->pusher->trigger($chatchannel_user, 'new-message', $message);
    }
}
