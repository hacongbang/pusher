<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Carbon\Carbon;
//use App\Roles;
use App\Post;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\PostCreateRequest;
use App\Http\Requests\PostUpdateRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\App;
use App\Profile;

class PostController extends Controller {

    public function index() {
        $posts = Post::all();
               
        return view('admin.post.index', compact('posts'));
    }

    protected $fields = [
        'title' => '',
        'content' => '',
        'slug' => '',
    ];

    // Replace the create() method with this
    /**
     * Show form for creating new tag
     */
    public function create() {
        $data = [];
        foreach ($this->fields as $field => $default) {
            $data[$field] = old($field, $default);
        }

        return view('admin.post.create', $data);
    }

    public function store(PostCreateRequest $request) {
        $post = new Post();
        
        $user = Auth::user();
        $user_role = $user->roles()->lists('role_name')->all();
        
        foreach (array_keys($this->fields) as $field) {
            $post->$field = $request->get($field);
        }
        
        $post->user_id = $user->id;
        $post->save();
        
        $pusher = App::make('pusher');
        $pusher->trigger($user_role, 'insert', array('text' => 'User: "'.$user->name.'" just create 1 post have title: "'.$post->title.'"'));
        
        return redirect('/admin/post')
                        ->withSuccess("The post '$post->title' was created.");
    }

    public function edit($id) {
        $post = Post::findOrFail($id);
        $data = ['id' => $id];
        foreach (array_keys($this->fields) as $field) {
            $data[$field] = old($field, $post->$field);
        }

        return view('admin.post.edit', $data);
    }

        public function show($id)
    {
        //
    }

    
    public function update(Request $request, $id) {
       
        $post = Post::findOrFail($id);
        $user = Auth::user();

        foreach ($this->fields as $field) {
            $post->$field = $request->get($field);
        }
        $post->user_id = $user->id;
  
        $post->save();

        return redirect("/admin/post/$id/edit")
                        ->withSuccess("Changes saved.");
    }
    
        /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Post::find($id);
        $role->delete();
        return redirect('/admin/post')->with('Delete success');
    }

}
