<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});
//Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

//Route::post('/user/login', [
//    'as' => 'login', 'uses' => 'LoginController@login'
//]);
//
//Route::get('/user/logout', [
//    'as' => 'logout', 'uses' => 'LoginController@logout'
//]);

// Edit profile
//Route::get('profile/register', 'ProfileController@create');
Route::post('profile/store', 'ProfileController@store');
//
$router->group([
    'namespace' => 'Admin',
    'middleware' => 'auth',
], function () { 
    resource('admin/role', 'RoleController');
    resource('admin/user', 'UserController');
    resource('admin/permission', 'PermissionController');
    resource('admin/post', 'PostController');
    Route::controller('chat', 'ChatController');
});

// $router->group([
//     'middleware' => 'auth',
// ], function () {
//     resource('profile', 'ProfileController');
// });

Route::get('auth/facebook', 'Auth\AuthController@redirectToProviderFacebook');
Route::get('auth/facebook/callback', 'ProfileController@loginWithFacebook');
Route::get('auth/google', 'Auth\AuthController@redirectToProviderGoogle');
Route::get('auth/google/callback', 'ProfileController@loginWithGoogle');

Route::get('/profile/register/activate/{code}', [
    'as' => 'activate', 'uses' => 'ProfileController@activeAccount'
]);
Route::get('profile/register', [
	'as' => 'aaa',
	
	'uses' => 'ProfileController@create']);


use Illuminate\Support\Facades\App;

get('/bridge', function() {
    $pusher = App::make('pusher');

    $pusher->trigger( 'test-channel',
                      'test-event', 
                      array('text' => 'Preparing the Pusher Laracon.eu workshop!'));

    //return view('welcome');
});


//get('/bridge', function() {
//    $pusher = App::make('pusher');
//
//    $pusher->trigger( 'test-channel',
//                      'test-event', 
//                      array('text' => 'Preparing the Pusher Laracon.eu workshop!'));
//
//    return view('welcome');
//});


Route::controller('notifications', 'NotificationController');


//Route::get('chat', 'ChatController@index');
//Route::post('chat/message', 'ChatController@chatsend');