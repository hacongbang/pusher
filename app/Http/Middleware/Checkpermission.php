<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class Checkpermission
{
    public function handle($request, Closure $next, $permission = null)
    {


        if (!app('Illuminate\Contracts\Auth\Guard')->guest()) {

            if ($request->user()->hasPermission($permission)) {
                
                return $next($request);
            }
        }

        return $request->ajax ? response('Unauthorized.', 401) : redirect('/auth/login');
    }

}
