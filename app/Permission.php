<?php

namespace App;
use App\Services\Markdowner;
use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $table = 'permissions';

    protected $fillable = [
        'permission_title','permission_slug','permission_description'
    ];

    public function roles()    {
        return $this->belongsToMany('App\Roles');
    }
    /**
     * Sync role relation adding new roles as needed
     *
     * @param array roles
     */
    public function syncRoles(array $roles)
    {
        Roles::addNeededRoles($roles);
        if (count($roles)) {
            $this->roles()->sync(
                Roles::whereIn('id', $roles)->lists('id')->all()
            );
            return;
        }

        $this->roles()->detach();
    }
    public function getPermissionBySlug($slug){
        $result = Permission::where('permission_slug',$slug)->get();
        $result_array = $result->getDictionary();
        return array_shift($result_array);
    }
}
