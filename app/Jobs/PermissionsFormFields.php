<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Permission;
use App\Roles;
use Illuminate\Contracts\Bus\SelfHandling;

class PermissionsFormFields extends Job implements SelfHandling
{
    /**
     * The id (if any) of the Post row
     *
     * @var integer
     */
    protected $id;

    /**
     * List of fields and default value for each field
     *
     * @var array
     */
    protected $fieldList = [
        'permission_title' => '',
        'permission_slug' => '',
        'permission_description' => '',
        'roles' => [],
    ];

    /**
     * Create a new command instance.
     *
     * @param integer $id
     */
    public function __construct($id = null)
    {
        $this->id = $id;
    }

    /**
     * Execute the command.
     *
     * @return array of fieldnames => values
     */
    public function handle()
    {
        $fields = $this->fieldList;

        if ($this->id) {
            $fields = $this->fieldsFromModel($this->id, $fields);
        }

        foreach ($fields as $fieldName => $fieldValue) {
            $fields[$fieldName] = old($fieldName, $fieldValue);
        }

        return array_merge(
            $fields,
            ['allRoles' => Roles::lists('role_name','id')->all()]
        );
    }

    /**
     * Return the field values from the model
     *
     * @param integer $id
     * @param array $fields
     * @return array
     */
    protected function fieldsFromModel($id, array $fields)
    {
        $permission = Permission::findOrFail($id);

        $fieldNames = array_keys(array_except($fields, ['roles']));

        $fields = ['id' => $id];
        foreach ($fieldNames as $field) {
            $fields[$field] = $permission->{$field};
        }

        $fields['roles'] = $permission->roles()->lists('role_name')->all();

        return $fields;
    }
}
