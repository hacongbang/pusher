<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Profile;
use App\Roles;
use App\User;

use Illuminate\Contracts\Bus\SelfHandling;

class ProfileFormFields extends Job implements SelfHandling
{
    /**
     * The id (if any) of the Post row
     *
     * @var integer
     */
    protected $id;

    /**
     * List of fields and default value for each field
     *
     * @var array
     */
    protected $fields_user = [
        'name' => '',
        'email' => '',
        'password' => '',
    ];
    protected $fields_profile = [
        'firstname' => '',
        'lastname' => '',
        'phone_number' => '',
        'linked' => '',
        'address' => '',
    ];

    /**
     * Create a new command instance.
     *
     * @param integer $id
     */
    public function __construct($id = null)
    {
        $this->id = $id;
    }

    /**
     * Execute the command.
     *
     * @return array of fieldnames => values
     */
    public function handle()
    {
        $fields = [];

        if ($this->id) {


                $user = User::findOrFail($this->id);
                $user->profile->get();
                foreach (array_keys($this->fields_profile) as $field) {
                    $fields[$field] = old($field, $user->profile->$field);
                }
                $fields['id'] = $user->id;

        } else {
            $user = new User();
            $profile = new Profile();

            foreach (array_keys($this->fields_profile) as $field) {
                $fields[$field] = old($field, $profile->$field);
            }
        }
        foreach (array_keys($this->fields_user) as $field) {
            $fields[$field] = old($field, $user->$field);
        }

        return array_merge($fields);
    }
}
